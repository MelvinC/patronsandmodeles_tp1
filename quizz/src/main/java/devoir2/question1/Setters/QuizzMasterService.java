package devoir2.question1.Setters;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class QuizzMasterService {
	private JFrame ecran;
	private JPanel panneauCentral;
	private JRadioButton [] lesBoutons;
	private QuizzMaster quizzMaster = new CMMQuizz();
	private JLabel monlabel;
	private ButtonGroup groupe;
	private JPanel panneauChoix;
	private ActionListener listener;
	public QuizzMasterService()
	{
		this.ecran = new JFrame();
		lesBoutons = new JRadioButton [4];
		ecran.setSize( 450, 200 );
		this.ecran.setTitle(quizzMaster.getType());
		// Fermeture de la fen�tre
		ecran.addWindowListener( new WindowAdapter() {
		public void windowClosing( WindowEvent we ) {
			QuizzMasterService.this.terminer();
		}
		} );
		// Le panneau central
		this.panneauCentral = new JPanel( new BorderLayout() );
		ecran.add( panneauCentral );
		this.listener = new ActionListener() {
	         public void actionPerformed (ActionEvent e) {
	        	 AbstractButton aButton = (AbstractButton) e.getSource();
	        	 String message=aButton.getText();
	        	 if(message.equals(quizzMaster.getChoix(quizzMaster.getReponse())))
	        		 JOptionPane.showMessageDialog( ecran, "bonne r�ponse" );
	        	 else
	        		 JOptionPane.showMessageDialog( ecran, "mauvaise r�ponse" );         
	        	 
	         };
	      };
		JPanel panneauQuestion= new JPanel( new FlowLayout( FlowLayout.LEADING ) );
		panneauQuestion.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder()));
		this.monlabel=new JLabel(quizzMaster.getQuestion());
		panneauQuestion.add(monlabel);
		panneauCentral.add( panneauQuestion, BorderLayout.NORTH );
		this.panneauChoix = new JPanel( new FlowLayout( FlowLayout.LEFT ) );
		this.groupe = new ButtonGroup ();
		for(int i=0; i<4; i++)
		{
			lesBoutons[i] = new JRadioButton (quizzMaster.getChoix(i));
			groupe.add(lesBoutons[i]);
			panneauChoix.add(lesBoutons[i]);
			lesBoutons[i].addActionListener(listener);
		}
		panneauCentral.add( panneauChoix, BorderLayout.CENTER);
		ecran.setVisible( true );
	}
	private void terminer() {
		
		this.ecran.dispose();			
		System.exit( 0 );
	}
	
	public void setQuizzMaster(QuizzMaster qm) {
		this.panneauCentral.remove(this.panneauChoix);
		this.quizzMaster = qm;
		this.monlabel.setText(quizzMaster.getQuestion());
		this.groupe = new ButtonGroup ();
		this.panneauChoix = new JPanel( new FlowLayout( FlowLayout.LEFT ) );
		for(int i=0; i<4; i++)
		{
			lesBoutons[i] = new JRadioButton (quizzMaster.getChoix(i));
			groupe.add(lesBoutons[i]);
			panneauChoix.add(lesBoutons[i]);
			lesBoutons[i].addActionListener(listener);
		}
		panneauCentral.add( panneauChoix, BorderLayout.CENTER);
		ecran.setVisible( true );
	}
	
	public QuizzMaster getQuizzMaster() {
		return this.quizzMaster;
	}
}
