package devoir2.question1.Setters;

public interface QuizzMaster {
	public String getQuestion ();
	public String getChoix (int indice);
	public int getReponse ();
	public String getType ();

}
