package devoir2.question1.Constructors;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class QuizProgram {

	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("UMLQuizzConstructor.xml");
		QuizzMasterService quizzMasterService = (QuizzMasterService) context.getBean("Choix");
	}

}
