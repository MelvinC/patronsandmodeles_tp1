package Aspects;

import java.util.LinkedList;
import java.util.List;

import Application.Billboard;
import Application.Train;
import Application.Date;

public aspect ATrain {

	private List<Billboard> Train.billboards = new LinkedList<Billboard>();

	public void Train.addBillboard(Billboard b) {
		this.billboards.add(b);
	}

	public void Train.removeBillboard(Billboard b) {
		this.billboards.remove(b);
	}

	pointcut addBillboard(Billboard b, Train t) : execution(void Billboard.addTrain(Train)) && this(b) && args(t);

	after(Billboard b, Train t): addBillboard(b,t){
		t.addBillboard(b);
	}

	pointcut removeBillboard(Billboard b, Train t) : execution(void Billboard.removeTrain(Train)) && this(b) && args(t);

	after(Billboard b, Train t): removeBillboard(b,t){
		t.removeBillboard(b);
	}

//	after():initialization(Train.new(..)){
//		Train t = (Train) thisJoinPoint.getTarget();
//	}

	pointcut updateDelay(Train t): execution(void Train.delay(Date)) && this(t);
	after(Train t):updateDelay(t){
		for(Billboard b : t.billboards) {
			b.setText();
		}
	}
}
