package Application;

public class Date {

	private int hour;
	private int minutes;
	
	public Date(int hour, int minutes){
		this.hour = hour;
		this.minutes = minutes;
	}
	
	public void delay(Date delay) {
		int deltaHour = 0;
		if(delay.minutes + this.minutes > 59) {
			deltaHour += 1;
			this.minutes = delay.minutes + this.minutes - 60;
		}
		else if (delay.minutes + this.minutes < 0) {
			deltaHour -= 1;
			this.minutes = 60 + delay.minutes + this.minutes;
		}
		else {
			this.minutes = delay.minutes + this.minutes;
		}
		if(this.hour + delay.hour + deltaHour > 23) {
			this.hour = this.hour + delay.hour + deltaHour - 24;
		}
		else if (this.hour + delay.hour + deltaHour < 0) {
			this.hour = 24- this.hour + delay.hour + deltaHour;
		}
		else {
			this.hour = delay.hour + this.hour + deltaHour;
		}
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(((Integer)this.hour).toString().length()>1 ? this.hour : "0" + this.hour);
		sb.append(":");
		sb.append(((Integer)this.minutes).toString().length()>1 ? this.minutes : "0" + this.minutes);
		return sb.toString();
	}
}
