package Application;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.util.LinkedList;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class Station {
	
	private JFrame window;
	private JPanel window_panel;
	private JPanel train_panel;
	private JPanel delay_panel;
	private JComboBox<Train> train_choose;
	private JTextField hours;
	private JTextField minutes;
	private JButton delay;
	private JLabel h;
	
	private List<Train> trains;
	
	public Station() {
		this.trains = new LinkedList<Train>();
		this.setGraphic();
	}
	
	public void addTrain(Train t) {
		this.trains.add(t);
		this.train_choose.addItem(t);
		this.train_choose.repaint();
	}
	
	public void removeTrain(Train t) {
		this.trains.remove(t);
		this.train_choose.removeItem(t);
		this.train_choose.repaint();
	}
	
	public void setGraphic() {
		this.window = new JFrame("Station");
		this.window.setSize(new Dimension(500, 200));
		this.window_panel = new JPanel();
		this.window_panel.setLayout(new BoxLayout(this.window_panel, BoxLayout.Y_AXIS));
		this.train_panel = new JPanel();
		this.delay_panel = new JPanel(new FlowLayout());
		
		this.train_choose = new JComboBox<Train>();
		for(Train t : this.trains) {
			this.train_choose.addItem(t);
		}
		this.train_choose.setPreferredSize(new Dimension(250, 50));
		this.train_panel.add(this.train_choose);
		this.train_panel.setSize(new Dimension(500,100));
		
		this.hours = new JTextField();
		this.hours.setPreferredSize(new Dimension(50,20));
		this.hours.setText("00");
		this.hours.setHorizontalAlignment(SwingConstants.RIGHT);
		this.minutes = new JTextField();
		this.minutes.setPreferredSize(new Dimension(50,20));
		this.minutes.setText("00");
		this.minutes.setHorizontalAlignment(SwingConstants.RIGHT);
		this.h = new JLabel("h");
		this.delay = new Delay_Button("delay", this);
		this.delay.setPreferredSize(new Dimension(100,20));
				
		this.delay_panel.add(this.hours);
		this.delay_panel.add(this.h);
		this.delay_panel.add(this.minutes);
		this.delay_panel.add(this.delay);
		this.delay_panel.setSize(new Dimension(500,100));
		
		this.window_panel.add(this.train_panel);
		this.window_panel.add(this.delay_panel);
		this.window.setContentPane(this.window_panel);
		this.window.setVisible(true);
	}
	
	public void delay() {
		Train t = (Train) this.train_choose.getSelectedItem();
		Date d = new Date(Integer.parseInt(this.hours.getText()), Integer.parseInt(this.minutes.getText()));
		System.out.println(d);
		System.out.println(t.toCompleteString());
		t.delay(d);
		System.out.println(t.toCompleteString());
	}
	
	public static void main(String[] args) {
		Train t = new Train(new Date(12, 04), new Date(5, 25), 01, "Paris", "Lyon");
		System.out.println(t);
		Billboard b = new Large_Billboard("Quai");
		b.addTrain(t);
		Train t2 = new Train(new Date(2, 8), new Date(5, 35), 02, "Mulhouse", "Toulouse");
		b.addTrain(t2);
		t.delay(new Date(1, 25));
		//b.setText();
		
		Billboard b2 = new Little_Billboard("caisse");
		b2.addTrain(t);
		b2.addTrain(t2);
		
		Station s = new Station();
		s.addTrain(t);
		s.addTrain(t2);
	}
}