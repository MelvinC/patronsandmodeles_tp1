package Application;

public class Train{

	private Date departure;
	private Date arrival;
	private int number;
	private String departureTown;
	private String arrivalTown;
	
	public Train(Date departure, Date arrival, int number, String departureTown, String arrivalTown) {
		this.departure = departure;
		this.arrival = arrival;
		this.number = number;
		this.departureTown = departureTown;
		this.arrivalTown = arrivalTown;
	}
	
	public void changeDeparture(Date departure) {
		this.departure = departure;
	}
	
	public void changeArrival(Date arrival) {
		this.arrival = arrival;
	}
	
	public void delay(Date delay) {
		this.arrival.delay(delay);
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Train n�");
		sb.append(this.number);
		return sb.toString();
	}
	
	public String toCompleteString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Train n�");
		sb.append(this.number);
		sb.append(" parti � : ");
		sb.append(this.departure);
		sb.append(" arrive � : ");
		sb.append(this.arrival);
		sb.append(", D�part : ");
		sb.append(this.departureTown);
		sb.append(", Destination : ");
		sb.append(this.arrivalTown);
		return sb.toString();
	}
}
