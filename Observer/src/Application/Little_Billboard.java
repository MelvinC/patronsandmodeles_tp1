package Application;
import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;

public class Little_Billboard extends Billboard{

	public Little_Billboard(String name) {
		super(name);
	}

	public void setGraphic() {
		this.window = new JFrame(this.name);
		this.window.setSize(new Dimension(500, 300));
		this.panel = new JPanel();
		this.label = new JTextArea();
		this.label.setEditable(false);
		this.panel.add(label);
		this.window.add(panel);
		this.window.setVisible(true);		
	}
	
	public void setText() {
		StringBuilder sb = new StringBuilder();
		for(Train t : trains) {
			sb.append(t.toCompleteString());
			sb.append("\n");
		}
		this.label.setText(sb.toString());
		this.label.setFont(this.label.getFont().deriveFont(12f));
		this.label.repaint();
	}
}
