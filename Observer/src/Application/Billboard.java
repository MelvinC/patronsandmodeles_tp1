package Application;
import java.awt.Dimension;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

public abstract class Billboard {

	protected String name;
	protected List<Train> trains;
	
	protected JFrame window;
	protected JPanel panel;
	protected JTextArea label;
	
	public Billboard(String name) {
		this.name = name;
		this.trains = new LinkedList<Train>();
		setGraphic();
	}
	
	public abstract void setGraphic();
	
	public void addTrain(Train t) {
		this.trains.add(t);
		this.setText();
	}
	
	public void removeTrain(Train t) {
		this.trains.remove(t);
		this.setText();
	}
	
	public abstract void setText();
}
