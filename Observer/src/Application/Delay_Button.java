package Application;
import java.awt.event.MouseEvent;

import javax.swing.JButton;

public class Delay_Button extends JButton {
	
	private static final long serialVersionUID = 1L;
	protected boolean mousePressed = false;
	protected boolean mouseOver = false;
	
	private Station station;
	
	public Delay_Button(String name, Station station) {
		super(name);
		this.station = station;
	}

	protected void processMouseEvent(MouseEvent e) {
		super.processMouseEvent(e);
		if (e.getID() == MouseEvent.MOUSE_PRESSED) {
			mousePressed = true;
			this.station.delay();
			this.repaint();
		}
		if (e.getID() == MouseEvent.MOUSE_RELEASED) {
			mousePressed = false;
			this.repaint();
		}
		if (e.getID() == MouseEvent.MOUSE_ENTERED) {
			mouseOver = true;
			this.repaint();
		}
		if (e.getID() == MouseEvent.MOUSE_EXITED) {
			mouseOver = false;
			this.repaint();
		}
	}
}
